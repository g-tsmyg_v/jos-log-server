import * as express from 'express';
import * as path from 'path';

import { apiRouter } from './api';
import { getLogsFromService } from './api/getlogs';
import { config } from './config';
import { cors } from './cors';
import { dashboard } from './dashboard/dashboard';
import { HttpRequestError } from './errors';
import { logger } from './services/logger';

const app = express();

app.set('views', path.join(__dirname, '../src/views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors);

app.use(express.static(`${config.ROOT_DIR}/public`));

app.use('/api', apiRouter);

app.get('/dashboard', dashboard);

app.get('/getlogs', getLogsFromService);

app.use('*', (req: express.Request, res: express.Response, next: express.NextFunction) => {
  next(new HttpRequestError({
    status: 400,
    message: 'Route not found',
  }));
});

// Express error handler
app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  logger.error(`${err.status || 500} - ${req.method} - ${req.originalUrl} - ${err.message} - ${req.ip}`);
  res.contentType('json');
  res.status(err.status || 500).send({
    message: err.message,
  });
});

export default app;
