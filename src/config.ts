import * as dotenv from 'dotenv';
import { BadServerConfigurationError } from './errors';

dotenv.config();

const {
  NODE_ENV,
  PORT,
  ROOT_DIR,
  PWD,
  AUTH_TOKEN,

  ADMIN_EMAIL,

  MAIL_HOST,
  MAIL_PORT,
  MAIL_SECURE,
  MAIL_AUTH_USER,
  MAIL_AUTH_PASSWORD,
  MAIL_DEFAULT_SENDER,

  LOG_ZIPPED,
  LOG_MAX_FILE_SIZE,
  LOG_MAX_FILE_DAYS,
} = process.env;

if (process.env.NODE_ENV === 'production') {
  if (!MAIL_HOST) {
    throw new BadServerConfigurationError('MAIL_HOST');
  }
  if (!MAIL_PORT) {
    throw new BadServerConfigurationError('MAIL_PORT');
  }
  if (!MAIL_SECURE) {
    throw new BadServerConfigurationError('MAIL_SECURE');
  }
  if (!MAIL_AUTH_USER) {
    throw new BadServerConfigurationError('MAIL_AUTH_USER');
  }
  if (!MAIL_AUTH_PASSWORD) {
    throw new BadServerConfigurationError('MAIL_AUTH_PASSWORD');
  }
  if (!MAIL_DEFAULT_SENDER) {
    throw new BadServerConfigurationError('MAIL_DEFAULT_SENDER');
  }
  if (!ADMIN_EMAIL) {
    throw new BadServerConfigurationError('ADMIN_EMAIL');
  }
}

export const config = {
  NODE_ENV,
  ROOT_DIR: ROOT_DIR || PWD,
  port: PORT || 8000,
  auth: {
    token: AUTH_TOKEN,
  },
  adminEmail: ADMIN_EMAIL,
  mail: {
    host: MAIL_HOST,
    port: MAIL_PORT,
    secure: MAIL_SECURE,
    user: MAIL_AUTH_USER,
    password: MAIL_AUTH_PASSWORD,
    defaultSender: MAIL_DEFAULT_SENDER,
  } as any,
  log: {
    zipped: LOG_ZIPPED === 'true',
    maxFileSize: LOG_MAX_FILE_SIZE || '20m', // k - kb, m - mb, g - gb
    maxFileDays: `${LOG_MAX_FILE_DAYS || 7}d`,
  }
};
