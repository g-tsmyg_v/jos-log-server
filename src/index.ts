import { config } from './config';

import * as http from 'http';

import app from './app';
import { logger } from './services/logger';


const server = http.createServer(app);

server.listen(config.port, () => {
  logger.warn(`Listening on ${config.port}`);
});
