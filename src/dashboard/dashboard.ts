import * as bluebird from 'bluebird';
import { Request, Response } from 'express';
import * as fs from 'fs';
// @ts-ignore
import * as JSONStream from 'JSONStream';
import * as moment from 'moment';

import { config } from '../config';
import { services } from '../services.json';
import { logger, NPM_LOG_LEVELS } from '../services/logger';
import { CombinedLogEntry, LogEntry } from '../api/getlogs';


export async function dashboard(req: Request, res: Response) {
  const selectedService = req.query.serviceName;
  const selectedLogLevel = req.query.logLevel;
  const dateFrom = req.query.dateFrom ? moment(req.query.dateFrom) : moment().subtract(1, 'days');
  const dateUntil = req.query.dateUntil ? moment(req.query.dateUntil) : moment();

  const errorsPath = `${config.ROOT_DIR}/logs/errors`;
  const errorFrom = moment().subtract(1, 'days');
  const errorUntil = moment();

  let errorLog: CombinedLogEntry[] = [];
  let serviceLog: LogEntry[] = [];

  errorLog = await readLogFiles(errorsPath, errorFrom, errorUntil) as CombinedLogEntry[];
  if (selectedService) {
    serviceLog = await readLogFiles(`${config.ROOT_DIR}/logs/${selectedService}`, dateFrom, dateUntil, selectedLogLevel);
  }

  res.render('index', {
    logLevelList: NPM_LOG_LEVELS,
    serviceList: services.map((service) => service.name),
    serviceLog,
    errorLog,
  });
}

async function readLogFiles(
  dirPath: string,
  dateFrom: moment.Moment,
  dateUntil: moment.Moment,
  logLevel?: string,
): Promise<LogEntry[] | CombinedLogEntry[]> {
  const log: LogEntry[] | CombinedLogEntry[] = [];

  let logFiles: string[] = [];
  try {
    logFiles = await bluebird.fromCallback((callback) => {
      fs.readdir(dirPath, callback);
    });
  } catch (e) {
    logger.error('Readddir error', e);
    throw new Error('Readdir error');
  }

  let needAdditionalFile = true;
  const logFilesToRender = logFiles.reverse().filter((fileName) => {
    const fileDate = moment(fileName.slice(0, -4));
    const shouldInclude = fileDate.isSameOrBefore(dateUntil)
      && (
        fileDate.isSameOrAfter(dateFrom)
        || (fileDate.isBefore(dateFrom) && needAdditionalFile)
      );
    if (fileDate.isBefore(dateFrom)) {
      needAdditionalFile = false;
    }
    return shouldInclude;
  });

  const logStreamPromises = logFilesToRender.map((fileName) => {
    const logStream = fs.createReadStream(`${dirPath}/${fileName}`, { autoClose: true });
    const jsonParseStream = logStream.pipe(JSONStream.parse('*'));
    jsonParseStream.on('data', (data: any) => {
      if (moment(data.timestamp).isAfter(dateUntil)) {
        jsonParseStream.end();
      }
      if (moment(data.timestamp).isSameOrAfter(dateFrom)) {
        if (data.level === logLevel || !logLevel) {
          log.push(data);
        }
      }
    });
    return new Promise((resolve) => {
      jsonParseStream.on('close', () => {
        resolve();
      });
    });
  });

  await Promise.all(logStreamPromises);

  log.sort((firstEntry, secondEntry) => {
    const firstEntryTimestamp = moment(firstEntry.timestamp);
    const secondEntryTimestamp = moment(secondEntry.timestamp);
    if (firstEntryTimestamp.isBefore(secondEntryTimestamp)) {
      return -1;
    }
    if (firstEntryTimestamp.isAfter(secondEntryTimestamp)) {
      return 1;
    }
    return 0;
  });

  return log;
}
