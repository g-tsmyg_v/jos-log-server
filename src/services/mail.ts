import * as nodemailer from 'nodemailer';
import { config } from '../config';
import { logger } from './logger';

export interface EmailData {
  emailSubject?: string;
  emailBody: string;
  emailSender?: string;
  emailRecipients: string[];
}

let transporter: any = null;

try {
  transporter = nodemailer.createTransport({
    host: config.mail.host,
    port: config.mail.port,
    secure: config.mail.secure,
    auth: {
      user: config.mail.user,
      pass: config.mail.password
    }
  });
} catch (e) {
  logger.error('Mail transport initialization error', e);
}

export async function sendEmail(data: EmailData) {
  const mailOptions = {
    from: `<${data.emailSender || config.mail.defaultSender}>`,
    to: data.emailRecipients,
    subject: data.emailSubject,
    text: data.emailBody,
    html: data.emailBody,
  };
  if (!transporter) {
    throw new Error('Mail transport not initialized');
  }
  try {
     await transporter.sendMail(mailOptions);
     return;
  } catch (e) {
    logger.error('Mail send error', e);
    throw new Error('Mail send error');
  }
}

export async function isTransporterInitialized() {
  try {
    await transporter.verify();
    return true;
  } catch (e) {
    return false;
  }
}
