// tslint:disable-next-line
require('./winston-workaround.js');

import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';

import { config } from '../config';


export const NPM_LOG_LEVELS = [
  'error',
  'warn',
  'info',
  'verbose',
  'debug',
  'silly'
];

export const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.json()
  ),
  transports: [
    new winston.transports.Console({
      level: 'warn'
    }),
    new DailyRotateFile({
      level: 'info',
      dirname: `${config.ROOT_DIR}/logs/log-service`,
      filename: 'application-%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      zippedArchive: config.log.zipped,
      maxSize: config.log.maxFileSize,
      maxFiles: config.log.maxFileDays,
      json: true,
    }),
  ],
});
