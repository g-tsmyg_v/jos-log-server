import axios from 'axios';
import * as bluebird from 'bluebird';
import { Request, Response } from 'express';
import * as fs from 'fs';
import * as mkdirp from 'mkdirp';
import * as moment from 'moment';
import * as zlib from 'zlib';

import { config } from '../config';

import { services } from '../services.json';
import { logger } from '../services/logger';
import { sendEmail } from '../services/mail';


export interface LogEntry {
  message: string;
  level: string;
  timestamp: string;
}

export interface CombinedLogEntry extends LogEntry {
  serviceName: string;
}

export async function getLogsFromService(req: Request, res: Response) {
  let errors: CombinedLogEntry[] = [];
  const promises = services.map(async (service) => {
    let httpResult: any;
    try {
      httpResult = await axios.get('http://localhost:8000/api/logs', {
        headers: {
          Authorization: 'Bearer D1BY4hJEdIjWgapjIoxAjWnUHKFIulXWHESUv96QLPXsQheW08CgdLmrFMKWF2xJ',
        },
        params: {
          zipFlag: 1,
        },
        responseType: 'arraybuffer',
      });
    } catch (e) {
      logger.error(`${service.name} is unavailable`, e);
    }

    try {
      if (httpResult && httpResult.data) {
        const serviceLogBuffer = await bluebird.fromCallback((callback) => zlib.unzip(httpResult.data, callback));
        const serviceLog = JSON.parse(serviceLogBuffer.toString());
        if (!Array.isArray(serviceLog) || serviceLog.length === 0) {
          return;
        }
        const firstEntry = serviceLog[serviceLog.length - 1];
        const serviceErrors: CombinedLogEntry[] = serviceLog.reduce((errorsFromLog: CombinedLogEntry[], entry: LogEntry) => {
          if (entry.level === 'error' || entry.level === 'crit') {
            errorsFromLog.push({
              ...entry,
              serviceName: service.name,
            });
          }
          return errorsFromLog;
        }, []);
        const serviceLogFolder = `${config.ROOT_DIR}/logs/${service.name}`;
        const logFileName = firstEntry.timestamp;
        // @ts-ignore
        await bluebird.fromCallback((callback) => mkdirp(serviceLogFolder, callback));
        await bluebird.fromCallback((callback => fs.appendFile(`${serviceLogFolder}/${logFileName}.log`, serviceLogBuffer, callback)));
        errors = [...errors, ...serviceErrors];
      }
    } catch (e) {
      logger.error(`${service.name} logs were not saved`);
    }
  });
  await Promise.all(promises);
  errors.sort((firstEntry, secondEntry) => {
    if (moment(firstEntry.timestamp).isBefore(secondEntry.timestamp)) {
      return -1;
    }
    if (moment(firstEntry.timestamp).isSame(secondEntry.timestamp)) {
      return 0;
    }
    return 1;
  });
  try {
    if (errors.length > 0) {
      const firstError = errors[errors.length - 1];
      const errorsBuffer = Buffer.from(JSON.stringify(errors));
      const errorsFolder = `${config.ROOT_DIR}/logs/errors`;
      const errorsFileName = `${errorsFolder}/${firstError.timestamp}.log`;
      // @ts-ignore
      await bluebird.fromCallback((callback) => mkdirp(errorsFolder, callback));
      await bluebird.fromCallback((callback => fs.appendFile(errorsFileName, errorsBuffer, callback)));

      if (config.adminEmail) {
        await sendEmail({
          emailSubject: 'Errors from services',
          emailBody: 'Some services have errors in their logs',
          emailRecipients: [config.adminEmail],
        });
      }
    }
  } catch (e) {
    logger.error(`Not able to save errors`);
  }

  res.send('ok');
}
