import { NextFunction, Request, Response } from 'express';
import { config } from '../../config';
import { HttpRequestError } from '../../errors';


export function auth(req: Request, res: Response, next: NextFunction) {
  const headerToken = req.get('Authorization');
  if (!headerToken) {
    return next(new HttpRequestError({
      status: 401,
      message: 'Not authorized'
    }));
  }

  const matches = headerToken.match(/[bB]earer\s(\S+)/);
  if (!matches) {
    return next(new HttpRequestError({
      status: 401,
      message: 'Not authorized'
    }));
  }

  const bearerToken = matches[1];
  if (bearerToken !== config.auth.token) {
    next(new HttpRequestError({
      status: 401,
      message: 'Not authorized',
    }));
  }

  next();
}
