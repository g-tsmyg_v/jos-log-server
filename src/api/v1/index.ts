import { Router } from 'express';
import { auth } from './auth';


const v1Router = Router();

v1Router.use(auth);

export { v1Router };
