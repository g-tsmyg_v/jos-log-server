import { Router } from 'express';

import { healthCheck } from './health';
import { getLogs } from './logs';
import { v1Router } from './v1';

const apiRouter = Router();

apiRouter.use('/v1', v1Router);

apiRouter.get('/logs', getLogs);
apiRouter.get('/health', healthCheck);

export { apiRouter };
